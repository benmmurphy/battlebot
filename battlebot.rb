#!/usr/bin/env ruby
require "rubygems"
require "bundler/setup"
require 'httparty'
require 'json'
require 'yaml'
require './hunt'
require './independent_distribution'
require 'set'

HIT = "hit"
MISS = "miss"
DESTROYED = "hit and destroyed"

def move(row, column)

  response = HTTParty.post("http://#{$host}/teams/#{$team_id}/game", :query => {'move' => [column,row]}, :headers => {"HTTP-MIDWAY-API-KEY" => $api_key})

  result = JSON.parse(response.body)

  #system('clear')
  #puts "#{result['move']} : #{result['status']}"
  #result['grid'].each do |grid_line|
  #  puts grid_line
  #end

  {:status => result['status'], :game_status => result["game_status"]}
end

def attack()

  distribution = Distribution.new
  state = BoardState.new

  visited = Set.new
  moves = 0
  results = []

  while true
    next_move = state.next_move
    from_distribution = false
    if next_move.nil?
      from_distribution = true
      distribution.reset
      distribution.fill_board(state.remaining_ships)
      next_move = distribution.most_likely
    end

    if visited.include?(next_move)
      raise "wtf visited: #{from_distribution} #{next_move} #{results.inspect}"
    end

    visited.add(next_move)
    result = move(next_move[0], next_move[1])

    results << [next_move, result]

    moves = moves + 1
    if result[:game_status] == "completed" 
      state.add_hit_destroyed(next_move[0], next_move[1])
      system('clear')
      puts state.dump
      #puts results.map {|x| x.inspect}.join("\n")
      return moves
    end

    if result[:status] == MISS
      distribution.add_miss(next_move[0], next_move[1])
      state.add_miss(next_move[0], next_move[1])
    elsif result[:status] == HIT
      distribution.add_hit(next_move[0], next_move[1])
      state.add_hit(next_move[0], next_move[1])
    elsif result[:status] == DESTROYED
      distribution.add_hit(next_move[0], next_move[1])
      state.add_hit_destroyed(next_move[0], next_move[1])
    else
      raise "wtf"
    end

    system('clear')
    puts state.dump
    puts "avg: #{$avg}"
  end

  puts "total moves: #{moves}"
end

config = YAML.load_file('team_config.yml')
$team_id = config['team_id']
$api_key = config['api_key']
$test = false
$host = config['host']

count = 200
moves = 0
results = []
$avg = 0
(1..count).each do |i|
  m = attack()
  results << m
  moves = moves + m
  $avg = moves / i.to_f
end

puts moves / count.to_f
puts results.inspect
