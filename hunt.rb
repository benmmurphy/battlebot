#!/usr/bin/env ruby
class BoardState

  ACROSS = [0,1]
  DOWN = [1,0]
  DIRECTIONS = [ACROSS, DOWN]

  UNKNOWN = 'U'
  HIT = 'H'
  MISS = 'M'
  DESTROYED = 'D'

  DEFAULT_SHIPS = [5, 4, 3, 3, 2]

  def initialize(width = 10, height = 10)
    @remaining_ships = DEFAULT_SHIPS.dup
    @board = (0...height).map { [UNKNOWN] * width }
  end

  def remaining_ships
    @remaining_ships
  end

  def size
    @board.size
  end

  def add_miss(row, column)
    @board[row][column] = MISS
  end

  def add_hit(row, column)
    @board[row][column] = HIT
  end

  def dump
    result = ""
    @board.each do |row|
      row.each do |cell|
        if cell == HIT
          result << "x"
        elsif cell == MISS
          result << "o"
        elsif cell == DESTROYED
          result << "D"
        else
          result << "."
        end
      end
      result << "\n"
    end
    result
  end

  def add_hit_destroyed(row, column)
    # find all valid combinations
    combos = find_destroyed_combos(row, column)
    #puts "combos: #{combos.inspect}"
    #intersect_destroyed_combos(combos)

    mark_all_hits_destroyed()
    @board[row][column] = DESTROYED

    #puts @board.inspect
    #puts @remaining_ships.inspect
  end

  def mark_all_hits_destroyed
    hits = 0
    @board.each do |row|
      row.each_with_index do |cell, idx|
        if cell == HIT
          row[idx] = DESTROYED
          hits = hits + 1
        end
      end
    end

    @remaining_ships = @remaining_ships - [hits]
  end

  def can_be_destroyed(row, column, offset, direction, ship)

    start_row = row + direction[0] * offset
    start_column = column + direction[1] * offset

    @board[row][column] = HIT

    if is_destroyed(start_row, start_column, direction, ship)
      @board[row][column] = MISS
      if !is_destroyed(start_row, start_column, direction, ship)
        return true
      end
    end
    
    return false
  end

  def is_destroyed(start_row, start_column, direction, ship)
    (0...ship).each do |offset|
      nr = offset * direction[0] + start_row
      nc = offset * direction[1] + start_column

      if nr < 0 || nr >= size
        return false
      end

      if @board[nr][nc] != HIT
        return false
      end
    end

    return true
  end

  def intersect_destroyed_combos(combos)
    h = Hash.new {|h,k| h[k] = 0}

    ships_h = Hash.new{|h,k| h[k] = 0}

    combos.each do |row, column, direction, size|
      (0...size).each do |i|
        pos = [row + i * direction[0], column + i * direction[1]]
        h[pos] = h[pos] + 1
      end

      ships_h[size] = ships_h[size] + 1
    end

    h.each do |k,v|
      if v == combos.length
        @board[k[0]][k[1]] = DESTROYED
      end
    end

    ships_h.each do |k,v|
      if v == combos.length
        @remaining_ships = @remaining_ships - [k]
      end
    end
  end

  def next_move
    moves = []
    (0...size).each do |row|
      (0...size).each do |col|
        if @board[row][col] == UNKNOWN 
          priority = neighbour_is_hit(row, col)
          if !priority.nil?
            moves << [priority, [row, col]]
          end
        end
      end
    end

    if moves.length == 0
      nil
    else
      best_priority = moves.sort[-1][0]
      (moves.select {|x| x[0] == best_priority}.sample)[1]
    end
  end

  def neighbour_is_hit(row, col)
    DIRECTIONS.each do |dir|
      [1,-1].each do |offset|
        nr = row + dir[0] * offset
        nc = col + dir[1] * offset
        if nr >= 0 && nr < size && nc >= 0 && nc < size && @board[nr][nc] == HIT
          nnr = row + dir[0] * offset * 2
          nnc = col + dir[1] * offset * 2
          if nnr >= 0 && nnr < size && nnc >= 0 && nnc < size
            if @board[nnr][nnc] == HIT
              return 1
            elsif @board[nnr][nnc] == MISS
              return -1
            else
              return 0
            end
          else
            return 0
          end
        end
      end
    end
    nil
  end

  def find_destroyed_combos(row, column)
    # XXXDXXX
    options = []
    @remaining_ships.uniq.each do |ship|
      (1...ship).each do |offset|
        if can_be_destroyed(row, column, offset, ACROSS, ship)
          options << [row, column + offset, ACROSS, ship]
        end

        if can_be_destroyed(row, column, -offset, ACROSS, ship)
          options << [row, column - offset, ACROSS, ship]
        end

        if can_be_destroyed(row, column, offset, DOWN, ship)
          options << [row + offset, column, DOWN, ship]
        end

        if can_be_destroyed(row, column, -offset, DOWN, ship)
          options << [row - offset, column, DOWN, ship]
        end
      end
    end
    options
  end
end

#state = BoardState.new
#state.add_miss(1,0)
#state.add_hit(0, 0)
#state.add_hit(0, 1)
#state.add_hit_destroyed(0, 2)

#state = BoardState.new
#state.add_hit(0,0)
#state.add_hit_destroyed(0,1)
#

#state = BoardState.new
#state.add_hit(2,6)
#state.add_miss(3,6)
#state.add_hit(2,7)
#puts state.next_move.inspect
