#!/usr/bin/env ruby

class Distribution
  ACROSS = [0,1]
  DOWN = [1,0]
  DIRECTIONS = [ACROSS, DOWN]
 
  DEFAULT_SHIPS = [5, 4, 3, 3, 2]
 
  def initialize(width = 10, height = 10)
    @hits = (0...height).map {[false] * width}
    @board = (0...height).map { [false] * width }
    @distribution = (0...height).map { [0] * width }
  end

  def size
    @board.size
  end

  def add_hit(row, column)
    @hits[row][column] = true
  end

  def add_miss(row, column)
    @board[row][column] = true
  end

  def can_write_ship(size, row, column, direction)
    (0...size).each do |d|
      new_row = row + direction[0] * d
      new_col = column + direction[1] * d
 
      if new_row >= @board.length || new_col >= @board[row].length || @board[new_row][new_col]
        return false
      end
    end
    true
  end

  def write_ship(size, row, column, direction)
    (0...size).each do |d|
      nrow = row + direction[0] * d
      ncol = column + direction[1] * d
      @distribution[nrow][ncol] = @distribution[nrow][ncol] + 1
    end
  end 

  def add_ship(size, row, column, direction)
    if can_write_ship(size, row, column, direction)
      write_ship(size, row, column, direction)
    end
  end

  def most_likely

    best = 0
    values = []
    (0...@board.size).each do |row|
      (0...@board.size).each do |col|
        if !@hits[row][col]
          if @distribution[row][col] > best
            best = @distribution[row][col]
            values = [[row, col]]
          elsif @distribution[row][col] == best
            values << [row, col]
          end
        end
      end
    end

    values.sample
  end

  def reset
    (0...size).each do |row|
      (0...size).each do |col|
        @distribution[row][col] = 0
      end
    end
  end

  def fill_board(ships = DEFAULT_SHIPS)
    DEFAULT_SHIPS.each do |ship|
      (0...@board.size).each do |row|
        (0...@board.size).each do |col|
          DIRECTIONS.each do |dir|
            add_ship(ship, row, col, dir)
          end
        end
      end
    end
  end
end



