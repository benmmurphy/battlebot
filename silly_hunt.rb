#!/usr/bin/env ruby
class BoardState

  ACROSS = [0,1]
  DOWN = [1,0]
  DIRECTIONS = [ACROSS, DOWN]

  UNKNOWN = 'U'
  HIT = 'H'
  MISS = 'M'
  DESTROYED = 'D'

  DEFAULT_SHIPS = [5, 4, 3, 3, 2]

  def initialize(width = 10, height = 10)
    @remaining_ships = DEFAULT_SHIPS.dup
    @board = (0...height).map { [UNKNOWN] * width }
  end

  def size
    @board.size
  end

  def add_miss(row, column)
    @board[row][column] = MISS
  end

  def add_hit(row, column)
    @board[row][column] = HIT
  end

  def add_hit_destroyed(row, column)
    @board[row][column] = DESTROYED
  end

  def neighbour_is_hit(row, col)
    DIRECTIONS.each do |dir|
      [1,-1].each do |offset|
        nr = row + dir[0] * offset
        nc = col + dir[1] * offset
        if nr >= 0 && nr < size && nc >= 0 && nc < size && @board[nr][nc] == HIT
          return true
        end
      end
    end

    false
  end

  def next_move
    (0...size).each do |row|
      (0...size).each do |col|
        if @board[row][col] == UNKNOWN && neighbour_is_hit(row, col)
          return [row, col]
        end
      end
    end

    nil
  end
end


